<?php
define('LOG_PATH', __DIR__.'/log');
if(!is_dir(LOG_PATH)){
    mkdir(LOG_PATH,0777,true);
}
session_save_path();
session_start();
set_time_limit(0);
define('NOW', $_SERVER['REQUEST_TIME']);
define('SELF_MD5', md5_file(__FILE__));

_Log('当前时间：'.NOW);
#获取目录下边所有文件
$ignore = array(
    '.','..','cache'
    );
$files = array();
$exts = array(
    'php','js','css','html','htm','tpl','sql','txt'
    );

function read($path)
{
    global $files,$exts,$ignore;
    _Log('------开始读取目录：'.$path.'------');
    $dir = scandir($path); 
    sleep(1);
    foreach ($dir as $file) {
        if ( in_array($file, $ignore ) ) {
            continue;
        }
        $file = $path . '/' . $file; 
        if (is_dir($file)) {
            read($file);
        } elseif (is_file($file) && md5_file($file) != SELF_MD5 ){
            _Log('读取到文件：'.$file);
            $info = pathinfo($file);
            if(isset($info['extension'])){
                $ext = $info['extension'];
                if(in_array($ext, $exts)){
                    _Log("开始替换文件内容",'note'); 
                    if(is_readable($file)){
                        $content = __Replace(file_get_contents($file));
                        _Log("内容替换完成！",'success'); 
                        $status = file_put_contents($file, $content);
                        if($status){
                            _Log("文件替换成功！",'success'); 
                        }else{
                            _Log('文件替换失败!','error');
                        }
                    }else{
                        _Log('文件不可读!','error');
                    }
                }else{
                 _Log("忽略掉文件：".$file,'low'); 
             }
         }else{
            _Log('文件（'.$file.'）信息获取失败','error');
        }
    }else{
        _Log("不能获取到文件（{$file}）的类型！",'error');
    }
}
}



read( dirname(__FILE__) );
_Log('所有文件替换完成！');






function __Replace($content){
    $content = str_replace('成都哈希曼普科技有限公司(www.hashmap.cn)','',  $content);
    $content = str_replace('www.bestfenxiao.com', 'www.hashmap.cn', $content);
    $content = str_replace('状元分销', '来咯旅游', $content);
    $content = str_replace('bestfenxiao.com', 'hashmap.cn', $content);
    $content = str_replace('bestfenxiao', 'hashmap', $content);
    $content = str_replace('pigcms', 'hashcms', $content);
    return $content;
}


/**
 * 显示日志
 * @param  [type] $msg    [description]
 * @param  string $status [description]
 * @return [type]         [description]
 */
function _Log($msg,$status = 'note'){
    static $init = false;
    if(!$init ){
        $init = true;
        echo '<!DOCTYPE html>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>数据导入</title>
            <style>
            *{padding:0;margin: 0;}
            #logbox{padding: 25px;}
                .log-item{padding: 10px 0;}
                .note{color:orange;}
                .error{color: red;}
                .success{color: green;}
                .low{color: green;}
                .date{color: #867373;margin-right: 10px;}
            </style>
        </head>
        <body>
            <div id="logbox"></div>
            <script>
                function Log($msg){
                    var _log = document.createElement("div");
                    _log.className="log-item";
                    _log.innerHTML = $msg;
                    document.getElementById("logbox").appendChild(_log);
                    window.scrollTo(0,window.document.body.scrollHeight);
                }
            </script>
        </body>
        </html>';
    }
    $date =date('Y-m-d H:i:s.t');
    $_msg = str_replace('"', '\\"', $msg) ;
    $_msg = '<span class=\"'.$status.'\"><span class=\"date\">'.LogDate().'</span>'.$_msg.'</span>';
    echo '<script>Log("'.$_msg.'")</script>';
    LogToFile($msg,$status);
    ob_flush();
    flush();
}
/**
 * 将日志写入文件
 * @param String $msg    日志内容
 * @param String $status 状态
 */
function LogToFile($msg,$status)
{
    $file = LOG_PATH.'/log.'.NOW.'.txt';
    $log = LogDate().' ['.$status.'] '.$msg;
    file_put_contents($file, $log."\n",FILE_APPEND);
}
/**
 * 获取到记录log的时间戳
 */
function LogDate(){
    return date('Y-m-d H:i:s').'('.microtime(true).')';
}
